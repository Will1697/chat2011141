package com.kinal.williams.androidchat.login;

/**
 * Created by williams.
 */
public interface LoginInteractor {
    void checkSession();
    void doSignUp(String email, String password);
    void doSignIn(String email, String password);
}
