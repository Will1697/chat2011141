package com.kinal.williams.androidchat.addcontact;

/**
 * Created by williams.
 */
public interface AddContactRepository {
    void addContact(String email);
}
