package com.kinal.williams.androidchat.chat.events;

import com.kinal.williams.androidchat.entities.ChatMessage;

/**
 * Created by williams.
 */
public class ChatEvent {
    private ChatMessage message;

    public ChatMessage getMessage() {
        return message;
    }

    public void setMessage(ChatMessage message) {
        this.message = message;
    }
}
