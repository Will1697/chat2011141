package com.kinal.williams.androidchat.addcontact.ui;

/**
 * Created by williams.
 */
public interface AddContactView {
    void showInput();
    void hideInput();
    void showProgress();
    void hideProgress();

    void contactAdded();
    void contactNotAdded();
}
