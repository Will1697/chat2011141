package com.kinal.williams.androidchat.login;

import com.kinal.williams.androidchat.login.events.LoginEvent;

/**
 * Created by williams.
 */
public interface LoginPresenter {
    void onCreate();
    void onDestroy();
    void onResume();
    void onPause();
    void checkForAuthenticatedUser();
    void validateLogin(String email, String password);
    void registerNewUser(String email, String password);
    void onEventMainThread(LoginEvent event);
}
