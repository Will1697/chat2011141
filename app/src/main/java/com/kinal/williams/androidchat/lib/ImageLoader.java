package com.kinal.williams.androidchat.lib;

import android.widget.ImageView;

/**
 * Created by williams.
 */
public interface ImageLoader {
    void load(ImageView imgAvatar, String url);
}
