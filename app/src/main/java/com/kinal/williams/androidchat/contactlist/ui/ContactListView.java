package com.kinal.williams.androidchat.contactlist.ui;

import com.kinal.williams.androidchat.entities.User;

/**
 * Created by williams.
 */
public interface ContactListView {
    void onContactAdded(User user);
    void onContactChanged(User user);
    void onContactRemoved(User user);
}
