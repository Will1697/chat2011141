package com.kinal.williams.androidchat.addcontact;

import org.greenrobot.eventbus.Subscribe;

import com.kinal.williams.androidchat.addcontact.events.AddContactEvent;
import com.kinal.williams.androidchat.addcontact.ui.AddContactView;
import com.kinal.williams.androidchat.lib.EventBus;
import com.kinal.williams.androidchat.lib.GreenRobotEventBus;

/**
 * Created by williams.
 */
public class AddContactPresenterImpl implements AddContactPresenter {
    private EventBus eventBus;
    private AddContactView view;
    private AddContactInteractor interactor;

    public AddContactPresenterImpl(AddContactView view) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new AddContactInteractorImpl();
    }

    @Override
    public void onShow() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        view = null;
        eventBus.unregister(this);
    }

    @Override
    public void addContact(String email) {
        if(view != null){
            view.hideInput();
            view.showProgress();
        }
        interactor.execute(email);
    }

    @Override
    @Subscribe
    public void onEventMainThread(AddContactEvent event) {
        if(view != null){
            view.hideProgress();
            view.showInput();

            if(event.isError()){
                view.contactNotAdded();
            } else {
                view.contactAdded();
            }
        }
    }
}
