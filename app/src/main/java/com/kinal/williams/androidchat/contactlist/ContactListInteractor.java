package com.kinal.williams.androidchat.contactlist;

/**
 * Created by williams.
 */
public interface ContactListInteractor {
    void subscribe();
    void unsubscribe();
    void destroyListener();
    void removeContact(String email);
}
