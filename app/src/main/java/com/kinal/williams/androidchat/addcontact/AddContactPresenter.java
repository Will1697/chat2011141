package com.kinal.williams.androidchat.addcontact;

import com.kinal.williams.androidchat.addcontact.events.AddContactEvent;

/**
 * Created by williams.
 */
public interface AddContactPresenter {
    void onShow();
    void onDestroy();

    void addContact(String email);
    void onEventMainThread(AddContactEvent event);
}
