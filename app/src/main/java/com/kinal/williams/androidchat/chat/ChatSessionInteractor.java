package com.kinal.williams.androidchat.chat;

/**
 * Created by williams.
 */
public interface ChatSessionInteractor {
    void changeConnectionStatus(boolean online);
}
