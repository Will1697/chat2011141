package com.kinal.williams.androidchat.chat.ui;

import com.kinal.williams.androidchat.entities.ChatMessage;

/**
 * Created by williams.
 */
public interface ChatView {
    void onMessageReceived(ChatMessage msg);
}
