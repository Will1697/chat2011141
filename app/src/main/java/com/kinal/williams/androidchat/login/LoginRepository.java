package com.kinal.williams.androidchat.login;

/**
 * Created by williams.
 */
public interface LoginRepository {
    void signUp(String email, String password);
    void signIn(String email, String password);
    void checkSession();
}
