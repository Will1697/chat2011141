package com.kinal.williams.androidchat.contactlist;

import com.kinal.williams.androidchat.contactlist.events.ContactListEvent;

/**
 * Created by williams.
 */
public interface ContactListPresenter {
    void onPause();
    void onResume();
    void onCreate();
    void onDestroy();

    void signOff();
    String getCurrentUserEmail();
    void removeContact(String email);
    void onEventMainThread(ContactListEvent event);
}
