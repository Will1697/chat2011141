package com.kinal.williams.androidchat.contactlist;

/**
 * Created by williams.
 */
public interface ContactListSessionInteractor {
    void signOff();
    String getCurrentUserEmail();
    void changeConnectionStatus(boolean online);
}
