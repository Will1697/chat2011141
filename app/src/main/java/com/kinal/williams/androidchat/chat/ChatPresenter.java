package com.kinal.williams.androidchat.chat;

import com.kinal.williams.androidchat.chat.events.ChatEvent;

/**
 * Created by williams.
 */
public interface ChatPresenter {
    void onPause();
    void onResume();
    void onCreate();
    void onDestroy();

    void setChatRecipient(String recipient);
    void sendMessage(String msg);
    void onEventMainThread(ChatEvent event);
}
