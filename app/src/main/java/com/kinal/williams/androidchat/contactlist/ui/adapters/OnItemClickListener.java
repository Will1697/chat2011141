package com.kinal.williams.androidchat.contactlist.ui.adapters;

import com.kinal.williams.androidchat.entities.User;

/**
 * Created by williams.
 */
public interface OnItemClickListener {
    void onItemClick(User user);
    void onItemLongClick(User user);
}
