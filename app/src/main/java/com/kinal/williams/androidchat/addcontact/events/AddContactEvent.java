package com.kinal.williams.androidchat.addcontact.events;

/**
 * Created by williams.
 */
public class AddContactEvent {
    boolean error = false;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
