package com.kinal.williams.androidchat.addcontact;

/**
 * Created by williams.
 */
public interface AddContactInteractor {
    void execute(String email);
}
